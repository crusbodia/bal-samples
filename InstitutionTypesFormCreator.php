<?php
declare(strict_types=1);

namespace App\Core\FormsManager;

use App\Core\FormsManager\Constants\InstitutionTypes;
use App\Core\FormsManager\Interfaces\FormCreator;
use App\Core\FormsManager\Proxies\DetailedTargeting\District;
use App\Core\FormsManager\Proxies\DetailedTargeting\Institution;
use App\Core\FormsManager\Proxies\DetailedTargeting\School;
use App\Core\FormsManager\Traits\CheckStatus;
use App\Core\FormsManager\Traits\CriteriaParser;
use App\Models\ListingSegmentTargetingCriteria;
use App\Models\SegmentTargetingCriteriaFilterDetail;

/**
 * Class InstitutionTypesFormCreator
 *
 * @package App\Core\FormsManager
 */
class InstitutionTypesFormCreator implements FormCreator
{
    use CheckStatus, CriteriaParser;

    const RELATION_NAME = 'filters';
    const RELATION_VALUES_NAME = 'filter_values';

    /**
     * @var int
     */
    protected $institutionType;

    /**
     * @var array
     */
    protected $checkboxIds = [];

    /**
     * @var bool
     */
    private $withDetailed = false;

    /**
     * @var ListingSegmentTargetingCriteria|null
     */
    private $criteria = null;

    /**
     * InstitutionTypesFormCreator constructor.
     *
     * @param int|null $institutionType
     */
    public function __construct(?int $institutionType = null)
    {
        if ($institutionType !== null) {
            $this->institutionType = $institutionType;
        }
    }

    /**
     * @param ListingSegmentTargetingCriteria $criteria
     * @return array
     */
    public function createForm(ListingSegmentTargetingCriteria $criteria): array
    {
        $this->criteria = $criteria;

        $targetingControlId = $this->institutionType ?? request()->input('targetingControlId');
        $targetingControlId = $targetingControlId ? (int)$targetingControlId : null;

        $this->parseCriteria($criteria, static::RELATION_NAME, static::RELATION_VALUES_NAME);

        if ($targetingControlId !== null) {
            $detailedControlIds = $this->getDetailedControlIdsByInstitution($criteria->id, $targetingControlId);

            switch ($targetingControlId) {
                case InstitutionTypes::SCHOOLS:
                    return $this->prepareSchools($detailedControlIds);
                    break;
                case InstitutionTypes::DISTRICTS:
                    return $this->prepareDistricts($detailedControlIds);
                    break;
                case InstitutionTypes::INSTITUTIONS:
                    return $this->prepareInstitutions($detailedControlIds);
                    break;
                default:
                    return [];
                    break;
            }
        }
        $controlIdsSchools = $this->getDetailedControlIdsByInstitution(
            $criteria->id,
            InstitutionTypes::SCHOOLS
        );
        $controlIdsDistricts = $this->getDetailedControlIdsByInstitution(
            $criteria->id,
            InstitutionTypes::DISTRICTS
        );
        $controlIdsInstitutions = $this->getDetailedControlIdsByInstitution(
            $criteria->id,
            InstitutionTypes::INSTITUTIONS
        );

        $result[] = $this->prepareSchools($controlIdsSchools);
        $result[] = $this->prepareDistricts($controlIdsDistricts);
        $result[] = $this->prepareInstitutions($controlIdsInstitutions);

        return $result;
    }

    /**
     * @return void
     */
    public function setWithDetailed(): void
    {
        $this->withDetailed = true;
    }

    /**
     * @param array $detailedControlIds
     *
     * @return array
     */
    private function prepareSchools(array $detailedControlIds = []): array
    {
        $schoolsAll = [
            'checkboxId' => InstitutionTypes::SCHOOLS_CHECKBOX_ALL,
            'title' => 'All',
            'isAllButton' => true,
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $schoolsPublic = [
            'checkboxId' => InstitutionTypes::SCHOOLS_CHECKBOX_PUBLIC,
            'title' => 'Public/State/County Schools',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $schoolsPrivate = [
            'checkboxId' => InstitutionTypes::SCHOOLS_CHECKBOX_PRIVATE,
            'title' => 'Private Schools',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $schoolsCatholic = [
            'checkboxId' => InstitutionTypes::SCHOOLS_CHECKBOX_CATHOLIC,
            'title' => 'Catholic Schools',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $schoolsLibraries = [
            'checkboxId' => InstitutionTypes::SCHOOLS_CHECKBOX_LIBRARIES,
            'title' => 'K-12 Libraries',
            'type' => static::TYPE_LABEL,
            'description' => 'bla-bla-bla'
        ];

        $schools = [];
        $schools['title'] = 'K-12 Schools';
        $schools['targetingControlId'] = InstitutionTypes::SCHOOLS;
        $schools['data'][] = $schoolsAll;
        $schools['data'][] = $schoolsPublic;
        $schools['data'][] = $schoolsPrivate;
        $schools['data'][] = $schoolsCatholic;
        $schools['data'][] = $schoolsLibraries;

        $schools = $this->checkStatus($schools, 'targetingControlId');
        //$schools = $this->checkDisabled($schools, InstitutionTypes::SCHOOLS_CHECKBOX_ALL);

        if (!empty($detailedControlIds)) {
            $schools = $this->checkDisabledBySelectedDetailed(
                $schools,
                $detailedControlIds,
                InstitutionTypes::SCHOOLS
            );
        }

        if ($this->withDetailed) {
            $schools['detailedTargetingData'] = (new Manager(new School()))->create($this->criteria);
        }

        return $schools;
    }

    /**
     * @param array $detailedControlIds
     *
     * @return array
     */
    private function prepareDistricts(array $detailedControlIds = []): array
    {
        $districtsAll = [
            'checkboxId' => InstitutionTypes::DISTRICTS_CHECKBOX_ALL,
            'title' => 'All',
            'isAllButton' => true,
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $districtsPublic = [
            'checkboxId' => InstitutionTypes::DISTRICTS_CHECKBOX_PUBLIC,
            'title' => 'Public Districts',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $districtsCatholic = [
            'checkboxId' => InstitutionTypes::DISTRICTS_CHECKBOX_CATHOLIC,
            'title' => 'Catholic Diocese',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $districtsCounty = [
            'checkboxId' => InstitutionTypes::DISTRICTS_CHECKBOX_COUNTY,
            'title' => 'County/Regional Centers',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $districts = [];
        $districts['title'] = 'Districts';
        $districts['targetingControlId'] = InstitutionTypes::DISTRICTS;
        $districts['data'][] = $districtsAll;
        $districts['data'][] = $districtsPublic;
        $districts['data'][] = $districtsCatholic;
        $districts['data'][] = $districtsCounty;

        $districts = $this->checkStatus($districts, 'targetingControlId');
        //$districts = $this->checkDisabled($districts, InstitutionTypes::DISTRICTS_CHECKBOX_ALL);

        if (!empty($detailedControlIds)) {
            $districts = $this->checkDisabledBySelectedDetailed(
                $districts,
                $detailedControlIds,
                InstitutionTypes::DISTRICTS
            );
        }

        if ($this->withDetailed) {
            $districts['detailedTargetingData'] = (new Manager(new District()))->create($this->criteria);
        }

        return $districts;
    }

    /**
     * @param array $detailedControlIds
     *
     * @return array
     */
    private function prepareInstitutions(array $detailedControlIds = []): array
    {
        $institutionsAll = [
            'checkboxId' => InstitutionTypes::INSTITUTIONS_CHECKBOX_ALL,
            'title' => 'All',
            'isAllButton' => true,
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        // Removed from initial release. It'll be returned later
        //$institutionsColleges = [
        //    'checkboxId' => InstitutionTypes::INSTITUTIONS_CHECKBOX_COLLEGES,
        //    'title' => 'Colleges',
        //    'type' => static::TYPE_CHECKBOX,
        //    'status' => false,
        //    'disabled' => false
        //];

        $institutionsDaycares = [
            'checkboxId' => InstitutionTypes::INSTITUTIONS_CHECKBOX_DAYCARES,
            'title' => 'Daycare Centers',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $institutionsLibraries = [
            'checkboxId' => InstitutionTypes::INSTITUTIONS_CHECKBOX_LIBRARIES,
            'title' => 'Public Libraries',
            'type' => static::TYPE_CHECKBOX,
            'status' => false,
            'disabled' => false
        ];

        $collegeLibraries = [
            'checkboxId' => InstitutionTypes::INSTITUTIONS_CHECKBOX_COLLEGE_LIBRARIES,
            'title' => 'College Libraries',
            'type' => static::TYPE_LABEL,
            'description' => 'bla-bla-bla'
        ];

        $institutions = [];
        $institutions['title'] = 'Other Institutions';
        $institutions['targetingControlId'] = InstitutionTypes::INSTITUTIONS;
        $institutions['data'][] = $institutionsAll;
        //$institutions['data'][] = $institutionsColleges;
        $institutions['data'][] = $institutionsDaycares;
        $institutions['data'][] = $institutionsLibraries;
        $institutions['data'][] = $collegeLibraries;

        $institutions = $this->checkStatus($institutions, 'targetingControlId');
        //$institutions = $this->checkDisabled($institutions, InstitutionTypes::INSTITUTIONS_CHECKBOX_ALL);

        if (!empty($detailedControlIds)) {
            $institutions = $this->checkDisabledBySelectedDetailed(
                $institutions,
                $detailedControlIds,
                InstitutionTypes::INSTITUTIONS
            );
        }

        if ($this->withDetailed) {
            $institutions['detailedTargetingData'] = (new Manager(new Institution()))->create($this->criteria);
        }

        return $institutions;
    }

    /**
     * @TODO: Remove. Front check this now
     * @param array $form
     * @param int $allId
     *
     * @return array
     */
    //private function checkDisabled(array &$form, int $allId): array
    //{
    //    $ids = $this->checkboxIds[$form['targetingControlId']] ?? [];
    //    if (empty($ids)) {
    //        return $form;
    //    }
    //
    //    foreach ($form['data'] as &$data) {
    //        if (in_array($allId, $ids, true)) {
    //            if ($data['checkboxId'] !== $allId) {
    //                $data['disabled'] = true;
    //            }
    //        } else if ($data['checkboxId'] === $allId && count($ids) > 0) {
    //            $data['disabled'] = true;
    //        }
    //    }
    //
    //    return $form;
    //}

    /**
     * @param array $form
     * @param array $detailedControlIds
     * @param int $institutionType
     *
     * @return array
     */
    private function checkDisabledBySelectedDetailed(array &$form, array $detailedControlIds, int $institutionType): array
    {
        switch ($institutionType) {
            case InstitutionTypes::SCHOOLS:
                $detailedMap = InstitutionTypes::$detailedSchoolMap;
                break;
            case InstitutionTypes::DISTRICTS:
                $detailedMap = InstitutionTypes::$detailedDistrictMap;
                break;
            case InstitutionTypes::INSTITUTIONS:
                $detailedMap = InstitutionTypes::$detailedInstitutionMap;
                break;
            default:
                $detailedMap = [];
                break;
        }

        foreach ($form['data'] as &$data) {
            $itemsDetailed = $detailedMap[$data['checkboxId']];

            if ($detailedControlIds !== array_intersect($detailedControlIds, $itemsDetailed)) {
                $data['disabled'] = true;
                $data['disabledLabel'] = 'There are no leads in this institution with your current target criteria. ' .
                    'Consider broadening your targeting.';
            }
        }

        return $form;
    }

    /**
     * @param int $targetingCriteriaId
     * @param int $institutionType
     *
     * @return array
     */
    private function getDetailedControlIdsByInstitution(int $targetingCriteriaId, int $institutionType): array
    {
        $detailedControlIds = SegmentTargetingCriteriaFilterDetail::query()->where([
            ['listing_segment_targeting_criteria_id', $targetingCriteriaId],
            ['institution_type', $institutionType]
        ])->distinct('detailed_targeting_control')->pluck('detailed_targeting_control')->toArray();

        return $detailedControlIds;
    }
}