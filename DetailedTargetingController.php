<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Core\FormsManager\Constants\DetailedTargeting\District;
use App\Core\FormsManager\Constants\DetailedTargeting\Institution;
use App\Core\FormsManager\Constants\DetailedTargeting\School;
use App\Core\FormsManager\Constants\InstitutionTypes;
use App\Core\FormsManager\DetailedTargeting\DistrictFormCreator;
use App\Core\FormsManager\DetailedTargeting\InstitutionFormCreator;
use App\Core\FormsManager\DetailedTargeting\SchoolFormCreator;
use App\Core\FormsManager\Traits\ElementSaver;
use App\Http\Requests\SaveDetailedTargeting;
use App\Models\ListingSegmentTargetingCriteria;
use App\Models\SegmentTargetingCriteriaFilterDetail;
use App\Transformers\DetailedTargetingDistrictTransformer;
use App\Transformers\DetailedTargetingInstitutionTransformer;
use App\Transformers\DetailedTargetingSchoolTransformer;
use Illuminate\Http\JsonResponse;

/**
 * Class DetailedTargetingController
 *
 * @SuppressWarnings(PHPMD)
 *
 * @package App\Http\Controllers\API
 */
class DetailedTargetingController extends ApiController
{
    use ElementSaver;

    /**
     * @SWG\Get(
     *     path="/api/v1/targeting/institutions-detailed/school/{id}",
     *     x={
     *          "amazon-apigateway-integration": {
     *              "responses": {
     *                  "default": {
     *                      "statusCode": "200"
     *                  }
     *              },
     *              "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/school/{id}",
     *              "httpMethod": "GET",
     *              "type": "http_proxy",
     *              "requestParameters" : {
     *                  "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *                  "integration.request.path.id" : "method.request.path.id"
     *              }
     *          }
     *     },
     *     operationId="DetailedTargetingController.school",
     *     summary="Get school detailed targeting by targeting id",
     *     description="Detailed targeting for school",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Targeting ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param DetailedTargetingSchoolTransformer $transformer
     * @param ListingSegmentTargetingCriteria $pivot
     *
     * @return JsonResponse
     */
    public function school(
        DetailedTargetingSchoolTransformer $transformer,
        ListingSegmentTargetingCriteria $pivot
    ): JsonResponse {
        return $this->respond($transformer->transformWithForm($pivot));
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/targeting/institutions-detailed/school/{id}",
     *     x={
     *          "amazon-apigateway-integration": {
     *              "responses": {
     *                  "default": {
     *                      "statusCode": "200"
     *                  }
     *              },
     *              "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/school/{id}",
     *              "httpMethod": "POST",
     *              "type": "http_proxy",
     *              "requestParameters" : {
     *                  "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *                  "integration.request.path.id" : "method.request.path.id"
     *              }
     *          }
     *     },
     *     operationId="detailedTargetingController.saveSchool",
     *     summary="Save detailed targeting for school",
     *     description="Save detailed targeting for school",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="targeting ID",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="detailedTargetingControlId",
     *         in="body",
     *         description="Detailed targeting control ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="checkboxIds",
     *         in="body",
     *         description="checkbox IDs",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="slider",
     *         in="body",
     *         description="slider min and max values",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param ListingSegmentTargetingCriteria $pivot
     * @param DetailedTargetingSchoolTransformer $transformer
     * @param SaveDetailedTargeting $request
     *
     * @return JsonResponse
     */
    public function saveSchool(
        ListingSegmentTargetingCriteria $pivot,
        DetailedTargetingSchoolTransformer $transformer,
        SaveDetailedTargeting $request
    ): JsonResponse {
        $detailedControlId = (int)$request->input('detailedTargetingControlId');
        $checkboxIds = $request->input('checkboxIds');
        $slider = $request->input('slider');
        $formElementType = School::$targetingTypes[$detailedControlId];

        // Drop grades if we save types. Drop types if we save grades
        if ($detailedControlId === School::SCHOOL_TYPES) {
            SegmentTargetingCriteriaFilterDetail::query()->where([
                'listing_segment_targeting_criteria_id' => $pivot->id,
                'institution_type' => InstitutionTypes::SCHOOLS,
                'detailed_targeting_control' => School::GRADE_RANGE,
            ])->delete();
        } elseif ($detailedControlId === School::GRADE_RANGE) {
            SegmentTargetingCriteriaFilterDetail::query()->where([
                'listing_segment_targeting_criteria_id' => $pivot->id,
                'institution_type' => InstitutionTypes::SCHOOLS,
                'detailed_targeting_control' => School::SCHOOL_TYPES,
            ])->delete();
        }

        $pivot->filters()->update([
            'is_all' => false
        ]);

        $targetingDetails = SegmentTargetingCriteriaFilterDetail::query()->where([
            'listing_segment_targeting_criteria_id' => $pivot->id,
            'institution_type' => InstitutionTypes::SCHOOLS,
            'detailed_targeting_control' => $detailedControlId,
        ]);

        if ($checkboxIds === null && $slider === null) {
            //$targetingDetails->entity()->delete();
            $targetingDetails->delete();

            return $this->respond($transformer->transformWithForm($pivot));
        }

        if ($checkboxIds !== null && in_array(1, $checkboxIds, false) && count($checkboxIds) > 1) {
            return $this->respondBadRequest('Button All cannot be selected with any other');
        }

        if ($slider !== null && ($slider['min'] > $slider['max'])) {
            return $this->respondBadRequest('Max value has to be more than min');
        }

        switch ($formElementType) {
            case SchoolFormCreator::TYPE_CHECKBOX:
                $targetingDetails->delete();
                $value = $this->checkboxSaver($checkboxIds);
                break;
            case SchoolFormCreator::TYPE_SLIDER:
                $value = $this->sliderSaver(
                    (int)$slider['min'],
                    (int)$slider['max'],
                    $targetingDetails->first()->entity_id ?? null
                );
                break;
            default:
                return $this->respondBadRequest();
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
                $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
                $targetingDetails->institution_type = InstitutionTypes::SCHOOLS;
                $targetingDetails->detailed_targeting_control = $detailedControlId;
                $targetingDetails->entity()->associate($item);
                $targetingDetails->save();
            }

            return $this->respond($transformer->transformWithForm($pivot));
        }

        $targetingDetails = $targetingDetails->first();

        if ($targetingDetails === null) {
            $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
        }
        $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
        $targetingDetails->institution_type = InstitutionTypes::SCHOOLS;
        $targetingDetails->detailed_targeting_control = $detailedControlId;
        $targetingDetails->entity()->associate($value);
        $targetingDetails->save();

        return $this->respond($transformer->transformWithForm($pivot));
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/targeting/institutions-detailed/district/{id}",
     *     x={
     *          "amazon-apigateway-integration": {
     *              "responses": {
     *                  "default": {
     *                      "statusCode": "200"
     *                  }
     *              },
     *              "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/district/{id}",
     *              "httpMethod": "GET",
     *              "type": "http_proxy",
     *              "requestParameters" : {
     *                  "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *                  "integration.request.path.id" : "method.request.path.id"
     *              }
     *          }
     *     },
     *     operationId="DetailedTargetingController.district",
     *     summary="Get district detailed targeting by targeting id",
     *     description="Detailed targeting for district",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Targeting ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param DetailedTargetingDistrictTransformer $transformer
     * @param ListingSegmentTargetingCriteria $pivot
     *
     * @return JsonResponse
     */
    public function district(
        DetailedTargetingDistrictTransformer $transformer,
        ListingSegmentTargetingCriteria $pivot
    ): JsonResponse {
        return $this->respond($transformer->transformWithForm($pivot));
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/targeting/institutions-detailed/district/{id}",
     *     x={
     *          "amazon-apigateway-integration": {
     *              "responses": {
     *                  "default": {
     *                      "statusCode": "200"
     *                  }
     *              },
     *              "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/district/{id}",
     *              "httpMethod": "POST",
     *              "type": "http_proxy",
     *              "requestParameters" : {
     *                  "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *                  "integration.request.path.id" : "method.request.path.id"
     *              }
     *          }
     *     },
     *     operationId="detailedTargetingController.saveDistrict",
     *     summary="Save detailed targeting for school",
     *     description="Save detailed targeting for school",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="targeting ID",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="detailedTargetingControlId",
     *         in="body",
     *         description="Detailed targeting control ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="checkboxIds",
     *         in="body",
     *         description="checkbox IDs",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="slider",
     *         in="body",
     *         description="slider min and max values",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param ListingSegmentTargetingCriteria $pivot
     * @param DetailedTargetingDistrictTransformer $transformer
     * @param SaveDetailedTargeting $request
     *
     * @return JsonResponse
     */
    public function saveDistrict(
        ListingSegmentTargetingCriteria $pivot,
        DetailedTargetingDistrictTransformer $transformer,
        SaveDetailedTargeting $request
    ): JsonResponse {
        $detailedControlId = (int)$request->input('detailedTargetingControlId');
        $checkboxIds = $request->input('checkboxIds');
        $slider = $request->input('slider');

        $formElementType = District::$targetingTypes[$detailedControlId];

        $targetingDetails = SegmentTargetingCriteriaFilterDetail::query()->where([
            'listing_segment_targeting_criteria_id' => $pivot->id,
            'institution_type' => InstitutionTypes::DISTRICTS,
            'detailed_targeting_control' => $detailedControlId,
        ]);

        if ($checkboxIds === null && $slider === null) {
            $targetingDetails->delete();

            return $this->respond($transformer->transformWithForm($pivot));
        }

        if ($checkboxIds !== null && in_array(1, $checkboxIds, false) && count($checkboxIds) > 1) {
            return $this->respondBadRequest('Button All cannot be selected with any other');
        }

        if ($slider !== null && ($slider['min'] > $slider['max'])) {
            return $this->respondBadRequest('Max value has to be more than min');
        }

        switch ($formElementType) {
            case DistrictFormCreator::TYPE_CHECKBOX:
                $targetingDetails->delete();
                $value = $this->checkboxSaver($checkboxIds);
                break;
            case DistrictFormCreator::TYPE_SLIDER:
                $value = $this->sliderSaver(
                    (int)$slider['min'],
                    (int)$slider['max'],
                    $targetingDetails->first()->entity_id ?? null
                );
                break;
            default:
                return $this->respondBadRequest();
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
                $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
                $targetingDetails->institution_type = InstitutionTypes::DISTRICTS;
                $targetingDetails->detailed_targeting_control = $detailedControlId;
                $targetingDetails->entity()->associate($item);
                $targetingDetails->save();
            }

            return $this->respond($transformer->transformWithForm($pivot));
        }

        $targetingDetails = $targetingDetails->first();

        if ($targetingDetails === null) {
            $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
        }
        $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
        $targetingDetails->institution_type = InstitutionTypes::DISTRICTS;
        $targetingDetails->detailed_targeting_control = $detailedControlId;
        $targetingDetails->entity()->associate($value);
        $targetingDetails->save();

        return $this->respond($transformer->transformWithForm($pivot));
    }

    /**
     * @SWG\Get(
     *     path="/api/v1/targeting/institutions-detailed/institution/{id}",
     *     x={"amazon-apigateway-integration": {
     *          "responses": {
     *              "default": {
     *                  "statusCode": "200"
     *              }
     *          },
     *          "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/institution/{id}",
     *          "httpMethod": "GET",
     *          "type": "http_proxy",
     *          "requestParameters" : {
     *              "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *              "integration.request.path.id" : "method.request.path.id"
     *          }
     *      }},
     *     operationId="DetailedTargetingController.institution",
     *     summary="Get institution detailed targeting by targeting id",
     *     description="Detailed targeting for institution",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Targeting ID",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param DetailedTargetingInstitutionTransformer $transformer
     * @param ListingSegmentTargetingCriteria $pivot
     *
     * @return JsonResponse
     */
    public function institution(
        DetailedTargetingInstitutionTransformer $transformer,
        ListingSegmentTargetingCriteria $pivot
    ): JsonResponse {
        return $this->respond($transformer->transformWithForm($pivot));
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/targeting/institutions-detailed/institution/{id}",
     *     x={
     *          "amazon-apigateway-integration": {
     *              "responses": {
     *                  "default": {
     *                      "statusCode": "200"
     *                  }
     *              },
     *              "uri": "https://${stageVariables.balAPI}/targeting/institutions-detailed/institution/{id}",
     *              "httpMethod": "POST",
     *              "type": "http_proxy",
     *              "requestParameters" : {
     *                  "integration.request.header.x-welcome-token" : "stageVariables.welcomeToken",
     *                  "integration.request.path.id" : "method.request.path.id"
     *              }
     *          }
     *     },
     *     operationId="detailedTargetingController.saveInstitution",
     *     summary="Save detailed targeting for other institutions",
     *     description="Save detailed targeting for other institutions",
     *     tags={"detailed-institution-targeting"},
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="targeting ID",
     *         required=true,
     *         type="integer",
     *     ),
     *     @SWG\Parameter(
     *         name="detailedTargetingControlId",
     *         in="body",
     *         description="Detailed targeting control ID",
     *         required=true,
     *         type="integer",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="checkboxIds",
     *         in="body",
     *         description="checkbox IDs",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Parameter(
     *         name="slider",
     *         in="body",
     *         description="slider min and max values",
     *         required=false,
     *         type="array",
     *         @SWG\Schema(ref="#/definitions/TargetingCriteria")
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Success"
     *     )
     * )
     *
     * @param ListingSegmentTargetingCriteria $pivot
     * @param DetailedTargetingInstitutionTransformer $transformer
     * @param SaveDetailedTargeting $request
     *
     * @return JsonResponse
     */
    public function saveInstitution(
        ListingSegmentTargetingCriteria $pivot,
        DetailedTargetingInstitutionTransformer $transformer,
        SaveDetailedTargeting $request
    ): JsonResponse {
        $detailedControlId = (int)$request->input('detailedTargetingControlId');
        $checkboxIds = $request->input('checkboxIds');
        $slider = $request->input('slider');

        $formElementType = Institution::$targetingTypes[$detailedControlId];

        $targetingDetails = SegmentTargetingCriteriaFilterDetail::query()->where([
            'listing_segment_targeting_criteria_id' => $pivot->id,
            'institution_type' => InstitutionTypes::INSTITUTIONS,
            'detailed_targeting_control' => $detailedControlId,
        ]);
        if ($checkboxIds === null && $slider === null) {
            $targetingDetails->delete();

            return $this->respond($transformer->transformWithForm($pivot));
        }

        if ($checkboxIds !== null && in_array(1, $checkboxIds, false) && count($checkboxIds) > 1) {
            return $this->respondBadRequest('Button All cannot be selected with any other');
        }

        if ($slider !== null && ($slider['min'] > $slider['max'])) {
            return $this->respondBadRequest('Max value has to be more than min');
        }

        switch ($formElementType) {
            case InstitutionFormCreator::TYPE_CHECKBOX:
                $targetingDetails->delete();
                $value = $this->checkboxSaver($checkboxIds);
                break;
            case InstitutionFormCreator::TYPE_SLIDER:
                $value = $this->sliderSaver(
                    (int)$slider['min'],
                    (int)$slider['max'],
                    $targetingDetails->first()->entity_id ?? null
                );
                break;
            default:
                return $this->respondBadRequest();
        }

        if (is_array($value)) {
            foreach ($value as $item) {
                $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
                $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
                $targetingDetails->institution_type = InstitutionTypes::INSTITUTIONS;
                $targetingDetails->detailed_targeting_control = $detailedControlId;
                $targetingDetails->entity()->associate($item);
                $targetingDetails->save();
            }

            return $this->respond($transformer->transformWithForm($pivot));
        }

        $targetingDetails = $targetingDetails->first();

        if ($targetingDetails === null) {
            $targetingDetails = new SegmentTargetingCriteriaFilterDetail();
        }
        $targetingDetails->listing_segment_targeting_criteria_id = $pivot->id;
        $targetingDetails->institution_type = InstitutionTypes::INSTITUTIONS;
        $targetingDetails->detailed_targeting_control = $detailedControlId;
        $targetingDetails->entity()->associate($value);
        $targetingDetails->save();

        return $this->respond($transformer->transformWithForm($pivot));
    }
}