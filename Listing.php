<?php
declare(strict_types=1);

namespace App\Models;

use App\Core\Counter\Counter;
use App\Core\Listing\ListingReplicator;
use App\Core\Listing\ListingStatusProcessor;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;
use MDR\Components\Models\BaseModel;

/**
 * @SWG\Definition(
 *     title="Listing",
 *     type="object",
 * )
 *
 * @property int $id
 * @property int $type_id
 * @property int $user_id
 * @property string $name
 * @property int $count
 * @property int $status
 * @property array $status_value
 *
 * @property-read \Carbon\Carbon $created_at
 * @property-read \Carbon\Carbon $updated_at
 * @property-read \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon $activated_at
 * @property-read ListingType $type
 * @property-read User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ListingSegment[] $segments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ListingOrder[] $listingOrders
 * @property-read \Illuminate\Database\Eloquent\Collection|ListingFile[] $files
 * @property-read \Illuminate\Database\Eloquent\Collection|ListingVersion[] $versions
 * @property-read string $version_type
 *
 * Class Listing
 *
 * @package App\Models
 */
class Listing extends BaseModel
{
    use SoftDeletes;

    const TABLE_NAME = 'listings';

    const STATUS_SAVED_FOR_LATER = 0;
    const STATUS_DOWNLOAD_IN_PROCESS = 1; // @TODO move to a separate column or create a helper method
    const STATUS_EXPIRED = 2;
    const STATUS_ACTIVE = 3;
    const STATUS_PROCESSING = 4; // @TODO move to a separate column or create a helper method

    const RELATIONS_MAP = [
        'segments.targetingCriteriaPivot.filters',
        'segments.targetingCriteriaPivot.geographies',
        'segments.targetingCriteriaPivot.details',
        'segments.targetingCriteriaPivot.geographyDetails',
        'segments.targetingCriteriaPivot.demographics',
        'segments.targetingCriteriaPivot.jobFunctions',
        'segments.targetingCriteriaPivot.jobFunctionDetail'
    ];

    /**
     * @var ListingStatusProcessor
     */
    private $statusProcessor;

    /**
     * Listing constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->statusProcessor = new ListingStatusProcessor($this);
    }

    /**
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * @var array
     */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    /**
     * @var array
     */
    protected $dates = ['activated_at', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @var array
     */
    protected $casts = [
        'id' => 'int',
        'type_id' => 'int',
        'user_id' => 'int',
        'count' => 'int',
        'status' => 'int'
    ];


    /*
    |--------------------------------------------------------------------------
    |  Relations
    |--------------------------------------------------------------------------
    */

    /**
     * @return HasMany
     */
    public function segments(): HasMany
    {
        return $this->hasMany(ListingSegment::class, 'listing_id');
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return HasManyThrough
     */
    public function segmentCriterias(): HasManyThrough
    {
        return $this->hasManyThrough(
            ListingSegmentTargetingCriteria::class,
            ListingSegment::class,
            'listing_id',
            'listing_segment_id'
        );
    }

    /**
     * @return HasMany
     */
    public function versions(): HasMany
    {
        return $this->hasMany(ListingVersion::class);
    }

    /**
     * @return HasManyThrough
     */
    public function files(): HasManyThrough
    {
        return $this->hasManyThrough(
            ListingFile::class,
            ListingVersion::class,
            'listing_id',
            'listing_version_id'
        );
    }

    /**
     * @return HasMany
     */
    public function listingOrders(): HasMany
    {
        return $this->hasMany(ListingOrder::class);
    }

    /**
     * This method updates status to active and activated_at date
     * activated_at should be updated only on repurchase.
     *
     * @return void
     */
    public function setActiveOnRepurchase(): void
    {
        $this->status = self::STATUS_ACTIVE;
        $this->activated_at = Carbon::now();
    }

    /*
    |--------------------------------------------------------------------------
    |  Accessors
    |--------------------------------------------------------------------------
    */

    /**
     * @return ListingType
     */
    public function getTypeAttribute(): ListingType
    {
        return ListingType::get($this->type_id);
    }

    /*
   |--------------------------------------------------------------------------
   |  Mutators and Accessors
   |--------------------------------------------------------------------------
   */

    /**
     * @return array
     */
    public function getStatusValueAttribute(): array
    {
        return $this->statusProcessor->handle();
    }

    /**
     * @param int $status
     *
     * @return void
     */
    public function setStatusAttribute(int $status): void
    {
        $this->attributes['status'] = $status;

        if ($status === self::STATUS_ACTIVE && $this->activated_at === null) {
            $this->attributes['activated_at'] = Carbon::now();
        }
    }

//    /**
//     * @return string
//     * @throws \UnexpectedValueException
//     */
//    public function getVersionTypeAttribute(): string
//    {
//        return $this->files()->whereNull('subversion')->first()->getType();
//    }

    /*
    |--------------------------------------------------------------------------
    |  Other methods
    |--------------------------------------------------------------------------
    */

    /**
     * It's not obviously, but Counter triggers event that updates list's leads number (field: "count")
     *
     * @return void
     */
    public function refreshLeadsNumber(): void
    {
        (new Counter(Counter::ES_TYPE))->process($this);
    }

    /**
     * @return Listing
     */
    public function duplicate(): self
    {
        return (new ListingReplicator($this))->handle();
    }

    /**
     * @return int|null
     */
    public function getActivatedDaysLeft(): ?int
    {
        $now = Carbon::now();

        if ($this->status === self::STATUS_ACTIVE) {
            return ListingStatusProcessor::EXPIRES_AFTER - $now->diffInDays($this->activated_at);
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getExpiredDate(): ?int
    {
        if ($this->status === self::STATUS_EXPIRED) {
            return $this->activated_at->addDays(ListingStatusProcessor::EXPIRES_AFTER)->timestamp;
        }

        return null;
    }

    /**
     * 1 lead costs 1 credit.
     *
     * @return int Total leads number or difference between original file (if any) and current value
     */
    public function getCost(): int
    {
        $result = $this->count - $this->files()->where('version', 1)->value('count');

        return $result > 0 ? $result : 0;
    }
}